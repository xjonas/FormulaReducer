#include <iostream>
#include <string>
#include <z3++.h>
#include <cmath>
#include <fstream>
#include <cstdio>

#include "FormulaStats.h"
#include "FormulaTraverser.h"
#include "FormulaReducer.h"

using namespace std;
using namespace z3;

int main(int argc, char* argv[])
{
    char* filename;
    if (argc < 2)
    {
	std::cout << "Filename required" << std::endl;
	return 1;
    }
    else
    {
	filename = argv[1];
    }

    std::cout << filename << ",";

    z3::context ctx;
    z3::solver s(ctx);
    s.from_file(filename);
    z3::expr e = mk_and(s.assertions());

    FormulaTraverser<FormulaStats> traverser;
    traverser.Traverse(e);

    FormulaStats stats = traverser.GetData();
    if (stats.functionSymbols.find("concat") != stats.functionSymbols.end() ||
        stats.functionSymbols.find("extract") != stats.functionSymbols.end())
    {
        std::cout << "unsupported " << std::endl;
        std::remove(filename);
        return 1;
    }

    if (stats.maxBitWidth > 100)
    {
        std::cout << "tooLargeBW" << std::endl;
        std::remove(filename);
        return 0;
    }

    std::string fileNameStr(filename);

    for (int i = stats.maxBitWidth; i >= 1; i--)
    {
        std::string nameWithoutExt = fileNameStr.substr(0, fileNameStr.size() - 5);

        FormulaReducer reducer;
        s.reset();
        s.add(reducer.Reduce(e, i));

        std::ofstream file(nameWithoutExt + "_" + std::to_string(i) + "bits.smt2");
        if (file.is_open())
        {
            file << "(set-logic BV)" << std::endl;
            file << s.to_smt2() << std::endl;
            file.close();
        }
    }
    std::cout << "ok" << std::endl;
    std::remove(filename);
}
